[Boetti's map](http://www.moma.org/learn/moma_learning/alighiero-boetti-map-of-the-world-1989) using D3

### ToDo's:

Due to their peculiar bounding boxes (see Jason Davies [nice visualization](https://www.jasondavies.com/maps/bounds/)) some countries need special treatment

* France: split Guyana and map French flag accordingly
* USA: map the flag separately to [Contiguous USA](https://en.wikipedia.org/wiki/Contiguous_United_States) and Alaska (given the scale probably ignoring all off-shore [United States territories and possessions](https://en.wikipedia.org/wiki/Territories_of_the_United_States) will not be visible)
* Kosovo: cope with its lack of UN code in order to plot the flag.

For these special cases we could:

1. use a topojson file for the parts we are interested and replot mapping the relevant flag.<br>
   For USA: Alaska and Continental USA.<br>
   For France: Metropolitan France (i.e. includes Corsica) and Guiana
2. add an entry for Kosovo (special iso code and flag entry)

Inspired by [rveciana]](http://bl.ocks.org/rveciana/)'s block
'[Flag map with D3js - SVG](http://bl.ocks.org/rveciana/6193058)'
